<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test cases relevent to download an invoice copy</description>
   <name>Download_Invoice_Copy_TestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>sachinthasri@gmail.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>aa5bca6d-8d49-4823-bc9f-e6edd4d08b60</testSuiteGuid>
   <testCaseLink>
      <guid>bebfcb4b-0c81-4a19-8801-01b20237fe0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Download Invoice Copy Option/I_Want_An_Invoice_Copy_Download</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a08d6ee7-7613-470b-a721-d26215c6ebb2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/BasicData</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e3d00cfb-0735-4411-b532-5982ab25f48b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Download Invoice Copy Option/I_Want_An_Invoice_Copy_Page_Details_Verification</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ee4bc46b-8100-47e5-9746-6ff6489566ab</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>798b0311-6a29-4c26-af1c-a1bf048c315c</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
