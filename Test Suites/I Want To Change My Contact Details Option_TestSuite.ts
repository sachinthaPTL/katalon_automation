<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>I Want To Change My Contact Details Option_TestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e73e6dc1-ce64-463f-af6c-6cbd9e28dbb3</testSuiteGuid>
   <testCaseLink>
      <guid>82566a6c-8293-4db8-896f-ed3e4ffd1bf7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Want To Change My Contact Details Option/Change_My_Contact_Details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f737f85-d5d6-49b7-a802-67bd9b87ff14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Want To Change My Contact Details Option/Contact_Us_General_Feedback</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16711330-4381-4290-bd56-ef0c1b0c4b5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Want To Change My Contact Details Option/Contact_Us_Other</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9faa333-2262-4a49-bc24-24424bff3f7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Want To Change My Contact Details Option/Contact_Us_Part Payment_Delay Due Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e982e228-e08d-4fce-aa3f-ae4665aecb4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Want To Change My Contact Details Option/Contact_Us_Payment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5dbdc2f0-d02e-4118-ab3a-f2c9febf920d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Want To Change My Contact Details Option/Contact_Us_Payment_Objection_Complaint</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be4653d4-f71d-4447-85d8-27d7d51f0711</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Want To Change My Contact Details Option/Contact_Us_Personal_Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>798d5680-3d92-4227-8f8f-305e91cb8b0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Want To Change My Contact Details Option/Contact_Us_Without_Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b269372-2ef5-4f2e-84d7-fb9f1993e720</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Want To Change My Contact Details Option/Contact_Us_Without_Inquiry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be37a33b-ce4d-427a-a866-924cf7d00ad6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Want To Change My Contact Details Option/Contact_Us_Without_Mobile_Number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d7c5ab4-64a6-494d-828e-be083317d248</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Want To Change My Contact Details Option/Contact_Us_Without_Title</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16cb7bc3-3606-497b-a709-b7b96ddda2a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Want To Change My Contact Details Option/GDPR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>02d4d489-4114-42b1-a514-7a096c463e3c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Want To Change My Contact Details Option/Open_Privacy_Policy</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
