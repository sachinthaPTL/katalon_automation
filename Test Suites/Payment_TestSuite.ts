<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Payment_TestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e4fa14c3-e852-4ceb-bb44-4d64c52caba0</testSuiteGuid>
   <testCaseLink>
      <guid>c661466d-fb78-4575-b77e-d607d082d93f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/Card_Payment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f175ced3-dd15-4bbd-b318-a3baacdf6170</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/Card_Payment_Information</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ccfec48-216f-412a-ae31-8a719ac30d1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/Card_Payment_without_Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05babe4e-2a71-464d-beaf-9a392cdbf63d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payment/Card_Payment_without_Mobile_Number</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
