<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test cases relevent to Options in DP</description>
   <name>I_Have_An_Objections_TestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>sachinthasri@gmail.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>64543862-c98e-45ec-bb01-40b1f0c9584f</testSuiteGuid>
   <testCaseLink>
      <guid>58c34ed4-0855-4e34-bc22-d30eb678ecf9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Have An Objection Option/Objection_There_is_an_error_in_the_text_in_the_debt_collection_payment_request_letter_(Betalingsoppfordring)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1766aea-a6ab-46c6-8430-c8715ff8faa3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Have An Objection Option/Objection_There_is_an_error_in_the_text_in_the_debt_collection_warning</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7032bff1-a437-4155-930c-1e75201d40f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Have An Objection Option/Objection_The_due_date_of_the_payment_is_too_short_on_debt_collection_warning_or_debt_collection_payment_request_letter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7190c0fb-1b21-4459-b5ce-45aede0ff2e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Have An Objection Option/Objection_The_invoice_or_debt_collection_warning_was_paid_before_it_was_sent_to_debt_collection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f981dc7-12d1-4723-96ce-825b57280794</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Have An Objection Option/Objection_Without_Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0dbe87f2-17a7-4cd6-be2d-c24db0f13880</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Have An Objection Option/Objection_Without_MobileNumber</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c53d8774-6640-4936-b591-e04800072889</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Have An Objection Option/Objection_Without_MobileNumberAndEmail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fda07578-37ba-4c0d-9b26-a58bbec7b193</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Have An Objection Option/Objection_Without_Select_Invoice</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63707e45-b681-4e3e-8075-d93f0025d499</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Have An Objection Option/Objection_Without_Select_Reason</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77f1e134-a5a9-4338-afd9-5ab2759d367e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/I Have An Objection Option/Objection_With_Blank_Comment</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
