<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>registerCase</name>
   <tag></tag>
   <elementGuidId>92de5e28-c00e-4bbc-bcc1-f2b584848f77</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;HeaderInfo\&quot;: {\n    \&quot;PID\&quot;: \&quot;${creditornumber}\&quot;,\n    \&quot;FileType\&quot;: \&quot;DW\&quot;,\n    \&quot;FileCreator\&quot;: \&quot;Electricity\&quot;,\n    \&quot;FileVersion\&quot;: \&quot;1.0 \&quot;,\n    \&quot;GeneratorID\&quot;: \&quot;1252\&quot;,\n    \&quot;ErrorHandling\&quot;: \&quot;error\&quot;,\n    \&quot;MessageID\&quot;: \&quot;2934450B-F0BE-4F22-958D-0B8032236584\&quot;,\n    \&quot;InternalMessageID\&quot;: \&quot;\&quot;,\n    \&quot;ClaimsStatus\&quot;: \&quot;\&quot;,\n    \&quot;ClientCulture\&quot;: \&quot;en-us\&quot;,\n    \&quot;Checksums\&quot;: {\n      \&quot;NumberOfInvoices\&quot;: 1,\n      \&quot;NumberOfCustomers\&quot;: 1,\n      \&quot;TotalMainAmount\&quot;: 257.42\n    }\n  },\n  \&quot;CaseList\&quot;: [\n    {\n      \&quot;PID\&quot;: \&quot;${creditornumber}\&quot;,\n      \&quot;CID\&quot;: \&quot;124155\&quot;,\n      \&quot;CID2\&quot;: \&quot;12526\&quot;,\n      \&quot;CaseNumber\&quot;: \&quot;00245\&quot;,\n      \&quot;CaseDebtWarning\&quot;: \&quot;2018-06-11\&quot;,\n      \&quot;CaseType\&quot;: \&quot;C\&quot;,\n      \&quot;CaseReportGroup\&quot;: \&quot;group1\&quot;,\n      \&quot;CaseStatus\&quot;: \&quot;open\&quot;,\n      \&quot;PayNoteDate\&quot;: \&quot;2018-06-15\&quot;,\n      \&quot;CustomerList\&quot;: [\n        {\n          \&quot;PID\&quot;: \&quot;${creditornumber}\&quot;,\n          \&quot;CID\&quot;: \&quot;124155\&quot;,\n          \&quot;CType\&quot;: \&quot;Customer\&quot;,\n          \&quot;CLastName\&quot;: \&quot;Sendib\&quot;,\n          \&quot;CFirstName\&quot;: \&quot;${DebtorName}\&quot;,\n          \&quot;CDebtorType\&quot;: \&quot;P\&quot;,\n          \&quot;CBirthdate\&quot;: \&quot;1983-05-13\&quot;,\n          \&quot;CSocialSecurityNo\&quot;: \&quot;11058332986\&quot;,\n          \&quot;COrganisation\&quot;: \&quot;Org12\&quot;,\n          \&quot;CMobile\&quot;: \&quot;94717424060\&quot;,\n          \&quot;CHome\&quot;: \&quot;94717424060\&quot;,\n          \&quot;CWork\&quot;: \&quot;94717424060\&quot;,\n          \&quot;CMail\&quot;: \&quot;uautomationapk@gmail.com\&quot;,\n          \&quot;AddressList\&quot;: [\n            {\n              \&quot;PID\&quot;: \&quot;${creditornumber}\&quot;,\n              \&quot;CID\&quot;: \&quot;124155\&quot;,\n              \&quot;CAddressSource\&quot;: \&quot;post\&quot;,\n              \&quot;CAdress1\&quot;: \&quot;Park Lane Veg 1\&quot;,\n              \&quot;CAdress2\&quot;: \&quot;Terraso lane 321 \&quot;,\n              \&quot;CAdress3\&quot;: \&quot;24,railway avenue, colombo 7\&quot;,\n              \&quot;CZIP\&quot;: \&quot;6225\&quot;,\n              \&quot;CCity\&quot;: \&quot;STRAUMGJERDE\&quot;,\n              \&quot;CCountry\&quot;: \&quot;Sri Lanka\&quot;\n            }\n          ]\n        }\n      ],\n      \&quot;TransactionList\&quot;: [\n        {\n          \&quot;PID\&quot;: \&quot;${creditornumber}\&quot;,\n          \&quot;CID\&quot;: \&quot;124155\&quot;,\n          \&quot;TType\&quot;: \&quot;3\&quot;,\n          \&quot;TNumber\&quot;: \&quot;${Tnumber}\&quot;,\n          \&quot;TDate\&quot;: \&quot;2018-05-12\&quot;,\n          \&quot;TDueDate\&quot;: \&quot;2018-05-19\&quot;,\n          \&quot;TAmount\&quot;: \&quot;${MainAmount}\&quot;,\n          \&quot;TInfo\&quot;: \&quot;transatcion information\&quot;,\n          \&quot;TInterestRate\&quot;: 8.5,\n          \&quot;TInterestDate\&quot;: \&quot;2018-11-20\&quot;,\n          \&quot;TKid\&quot;: \&quot;1233242\&quot;,\n          \&quot;TCreditInvoice\&quot;: \&quot;123\&quot;,\n          \&quot;TStatus\&quot;: \&quot;Status1\&quot;,\n          \&quot;CustomSettings\&quot;: [\n            {\n              \&quot;ID\&quot;: 0,\n              \&quot;FieldID\&quot;: 0,\n              \&quot;Name\&quot;: \&quot;\&quot;,\n              \&quot;DisplayName\&quot;: \&quot;\&quot;,\n              \&quot;DataTypeID\&quot;: 0,\n              \&quot;Value\&quot;: \&quot;\&quot;,\n              \&quot;DefaultValue\&quot;: \&quot;\&quot;\n            }\n          ]\n        }\n      ],\n      \&quot;NoteList\&quot;: [\n        {\n          \&quot;PID\&quot;: \&quot;${creditornumber}\&quot;,\n          \&quot;CID\&quot;: \&quot;124155\&quot;,\n          \&quot;TNumber\&quot;: \&quot;12458265\&quot;,\n          \&quot;CaseNumber\&quot;: \&quot;12548\&quot;,\n          \&quot;NCreator\&quot;: \&quot;CGM\&quot;,\n          \&quot;NDate\&quot;: \&quot;2020-05-10\&quot;,\n          \&quot;NAction\&quot;: \&quot;No Action\&quot;,\n          \&quot;NDueDate\&quot;: \&quot;2020-05-10\&quot;,\n          \&quot;NText\&quot;: \&quot;NText For testing as\&quot;\n        }\n      ],\n      \&quot;PrintingInfo\&quot;: {\n        \&quot;PID\&quot;: \&quot;${creditornumber}\&quot;,\n        \&quot;CID\&quot;: \&quot;124155\&quot;,\n        \&quot;IOrderNumber\&quot;: \&quot;25887\&quot;,\n        \&quot;IOrderDate\&quot;: \&quot;2019-08-09\&quot;,\n        \&quot;IDeliveryNumber\&quot;: \&quot;587744\&quot;,\n        \&quot;IDeliveryName\&quot;: \&quot;CGM Delivery\&quot;,\n        \&quot;IDeliveryIdNo\&quot;: \&quot;25887\&quot;,\n        \&quot;IDeliveryAddress\&quot;: \&quot;Oslo Norway\&quot;,\n        \&quot;IVatAmount\&quot;: \&quot;10.5000\&quot;,\n        \&quot;IDiscountAmount\&quot;: \&quot;200.5600\&quot;\n      },\n      \&quot;IItemList\&quot;: [\n        {\n          \&quot;ICredRef\&quot;: \&quot;3b377643-3ffd-4326-9\&quot;,\n          \&quot;IProductNo\&quot;: \&quot;2dd\&quot;,\n          \&quot;IText\&quot;: \&quot;asdfa\&quot;,\n          \&quot;IText2\&quot;: \&quot;dasdasda\&quot;,\n          \&quot;INumberOfItems\&quot;: \&quot;1\&quot;,\n          \&quot;IItemUnit\&quot;: 1,\n          \&quot;IItemPrice\&quot;: \&quot;236.5200\&quot;,\n          \&quot;IAmount\&quot;: \&quot;236.5200\&quot;,\n          \&quot;IVatRate\&quot;: \&quot;10.5000\&quot;,\n          \&quot;IDiscountRate\&quot;: \&quot;24.2000\&quot;,\n          \&quot;IDiscountAmount\&quot;: \&quot;256.3200\&quot;,\n          \&quot;IExtraField1\&quot;: \&quot;IExtraField1 - Test1\&quot;,\n          \&quot;IExtraField2\&quot;: \&quot;IExtraField2 - Test2\&quot;,\n          \&quot;IExtraField3\&quot;: \&quot;IExtraField3 - Test3\&quot;,\n          \&quot;IExtraField4\&quot;: \&quot;IExtraField4 - Test4\&quot;\n        }\n      ]\n    }\n  ]\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://devboxweb/FDC_USBOX/CPBOXAPI/api/cases</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.token</defaultValue>
      <description></description>
      <id>4bc13504-8fb2-4d83-9dc7-d6d3df7bbf28</id>
      <masked>false</masked>
      <name>token</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.CreditorNumber</defaultValue>
      <description></description>
      <id>31821b72-c370-4313-87c6-46c7695bc838</id>
      <masked>false</masked>
      <name>creditornumber</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.Tnumber</defaultValue>
      <description></description>
      <id>7ff022b1-121c-4ddc-971b-b20d6d3fbf4f</id>
      <masked>false</masked>
      <name>Tnumber</name>
   </variables>
   <variables>
      <defaultValue>findTestData('BasicData').getValue(2, 1)</defaultValue>
      <description></description>
      <id>49e3d9d2-226b-4391-900f-be961334639a</id>
      <masked>false</masked>
      <name>DebtorName</name>
   </variables>
   <variables>
      <defaultValue>findTestData('BasicData').getValue(2, 2)</defaultValue>
      <description></description>
      <id>3e8e68ae-5a4e-44f4-a9f6-938d2e239196</id>
      <masked>false</masked>
      <name>MainAmount</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
