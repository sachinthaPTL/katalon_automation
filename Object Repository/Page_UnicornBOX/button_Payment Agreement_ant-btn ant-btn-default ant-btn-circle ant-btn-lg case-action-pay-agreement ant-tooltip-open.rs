<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Payment Agreement_ant-btn ant-btn-default ant-btn-circle ant-btn-lg case-action-pay-agreement ant-tooltip-open</name>
   <tag></tag>
   <elementGuidId>b5521a59-c0be-4b77-87a4-cb2bc8249975</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class='ant-btn ant-btn-default ant-btn-circle ant-btn-lg case-action-pay-agreement'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-btn ant-btn-default ant-btn-circle ant-btn-lg case-action-pay-agreement ant-tooltip-open</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/section[@class=&quot;ant-layout layout&quot;]/main[@class=&quot;ant-layout-content&quot;]/div[@class=&quot;content&quot;]/div[1]/main[@class=&quot;dom-bottom-space&quot;]/section[@class=&quot;ant-layout&quot;]/section[@class=&quot;ant-layout ant-layout-has-sider&quot;]/main[@class=&quot;ant-layout-content&quot;]/main[1]/div[@class=&quot;gutter-example&quot;]/div[@class=&quot;ant-row&quot;]/div[@class=&quot;ant-col ant-col-12 gutter-row&quot;]/div[@class=&quot;ant-row&quot;]/div[@class=&quot;ant-col ant-col-12 gutter-row&quot;]/div[@class=&quot;gutter-box pr-3&quot;]/div[2]/div[@class=&quot;case-action-list&quot;]/div[@class=&quot;d-flex flex-wrap case-action-list-body&quot;]/button[@class=&quot;ant-btn ant-btn-default ant-btn-circle ant-btn-lg case-action-pay-agreement ant-tooltip-open&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[15]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/section/main/div/div/main/section/section/main/main/div/div/div[2]/div/div/div/div[2]/div[2]/div[2]/button</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Payment Agreement'])[1]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Objection'])[1]/following::button[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='History'])[1]/preceding::button[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Doc type'])[1]/preceding::button[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/button</value>
   </webElementXpaths>
</WebElementEntity>
