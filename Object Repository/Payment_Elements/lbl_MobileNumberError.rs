<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_MobileNumberError</name>
   <tag></tag>
   <elementGuidId>1d581ca5-0718-45eb-8dbd-30edfa484537</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//mat-error[text()='Mobilnummer er nødvendig' or text()='Mobile number is required']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
