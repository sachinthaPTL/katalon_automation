<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>After</name>
   <tag></tag>
   <elementGuidId>ff3988ee-6ced-46b0-b740-0ce69f912051</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@class=&quot;btn btn-success btn-shaded btn-block btn-danger btn-error&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>d-flex justify-content-center ng-star-inserted</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-layout[@class=&quot;ng-tns-c1-0&quot;]/div[@class=&quot;ng-tns-c1-0 content dp-container&quot;]/app-invoice-copy[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;d-flex no-gutters case-history&quot;]/div[@class=&quot;col&quot;]/ul[@class=&quot;dp-case-list&quot;]/li[@class=&quot;separator ng-star-inserted&quot;]/div[@class=&quot;row no-gutters py-2&quot;]/div[@class=&quot;mr-3 pb-2 mt-1 col-md-auto&quot;]/div[@class=&quot;d-flex mt-4&quot;]/div[@class=&quot;mr-1&quot;]/app-progress-button[@class=&quot;btn-send-email&quot;]/button[@class=&quot;btn btn-success btn-shaded btn-block btn-danger btn-error&quot;]/div[1]/div[@class=&quot;d-flex justify-content-center ng-star-inserted&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='If you want invoice copy you can download it by pressing the invoice icon.'])[1]/following::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice Copy'])[1]/following::div[10]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice'])[1]/preceding::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Main Amount'])[3]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div/div/div/div/app-progress-button/button/div/div</value>
   </webElementXpaths>
</WebElementEntity>
