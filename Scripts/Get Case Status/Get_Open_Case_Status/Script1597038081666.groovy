import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils


import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper

String charset = ('0'..'9').join()

Integer length = 5

Integer tnumberlength = 4

def randomSring = RandomStringUtils.random(length, charset.toCharArray())

def tnumber = RandomStringUtils.random(tnumberlength, charset.toCharArray())

GlobalVariable.Tnumber = tnumber

CustomKeywords.'com.database.DBConnection.connectDB'(findTestData('BasicData').getValue(2, 5), findTestData('BasicData').getValue(2, 6), findTestData('BasicData').getValue(2, 7), findTestData('BasicData').getValue(2, 8))

def result = CustomKeywords.'com.database.DBConnection.executeQuery'('select top 1 * from EntRole where CreditorInkassoID is not null  order by 1 ASC')

while (result.next()) {
    Object caseno = result.getObject('CreditorInkassoID')

    //    if (caseno != null) {
    //        System.out.println('ExCaseNo ' + caseno)
    //    }
    GlobalVariable.CreditorNumber = caseno
}

def response = WS.sendRequest(findTestObject('API_Request/getAuth'))

JsonSlurper js = new JsonSlurper()

def sluper = new JsonSlurper()

def results = sluper.parseText(response.getResponseBodyContent())

WS.verifyResponseStatusCode(response, 200)

def token = results.access_token

GlobalVariable.token = token

println(token)

GlobalVariable.token = token

WS.sendRequest(findTestObject('API_Request/registerCase', [('token') : GlobalVariable.token]))

def ExCaseNumber = CustomKeywords.'com.database.DBConnection.executeQuery'('select top 1* from ar order by 1 desc')

while (ExCaseNumber.next()) {
    Object exCaseNumber = ExCaseNumber.getObject('ExCaseNo')

    //    if (caseno != null) {
    //        System.out.println('ExCaseNo ' + caseno)
    //    }
    GlobalVariable.ExcaseNumber = exCaseNumber
}

WebUI.openBrowser(GlobalVariable.DPBaseURL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('DP_LoginPage/drpLanguage'))

WebUI.click(findTestObject('DP_LoginPage/EN'))

WebUI.sendKeys(findTestObject('DP_LoginPage/txtCaseNumber'), GlobalVariable.ExcaseNumber)

WebUI.click(findTestObject('DP_LoginPage/btnCheckStatus'))

WebUI.verifyElementText(findTestObject('DP_LoginPage/lblOpenCase'), findTestData('Alerts').getValue(2, 1))

WebUI.closeBrowser()

