import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper

String charset = ('0'..'9').join()

Integer length = 5

Integer tnumberlength = 4

def randomSring = RandomStringUtils.random(length, charset.toCharArray())

def tnumber = RandomStringUtils.random(tnumberlength, charset.toCharArray())

GlobalVariable.Tnumber = tnumber

CustomKeywords.'com.database.DBConnection.connectDB'(findTestData('BasicData').getValue(2, 5), findTestData('BasicData').getValue(2, 6), findTestData('BasicData').getValue(2, 7), findTestData('BasicData').getValue(2, 8))


def result = CustomKeywords.'com.database.DBConnection.executeQuery'('select top 1 * from EntRole where CreditorInkassoID is not null  order by 1 ASC')

while (result.next()) {
    Object creditorinkassoid = result.getObject('CreditorInkassoID')

    //    if (caseno != null) {
    //        System.out.println('ExCaseNo ' + caseno)
    //    }
    GlobalVariable.CreditorNumber = creditorinkassoid
}

def response = WS.sendRequest(findTestObject('API_Request/getAuth'))

JsonSlurper js = new JsonSlurper()

def sluper = new JsonSlurper()

def results = sluper.parseText(response.getResponseBodyContent())

WS.verifyResponseStatusCode(response, 200)

def token = results.access_token

GlobalVariable.token = token

println(token)

GlobalVariable.token = token

WS.sendRequest(findTestObject('API_Request/registerCase', [('token') : GlobalVariable.token]))

def CaseNumber = CustomKeywords.'com.database.DBConnection.executeQuery'('select top 1 * from ar where Type=\'C\'order by 1 desc')

while (CaseNumber.next()) {
    Object caseNumber = CaseNumber.getObject('CaseNo')

    Object excaseNumber = CaseNumber.getObject('ExCaseNo')

    Object balance = CaseNumber.getObject('Balance')

    Object creditornumber = CaseNumber.getObject('CreditorEntRoleId')

    //Object IsObjected = CaseNumber.getObject('IsObjected')
    //Object DebtorObjectionReason = CaseNumber.getObject('DebtorObjectionReason')
    GlobalVariable.CaseNumber = caseNumber

    GlobalVariable.ExcaseNumber = excaseNumber

    GlobalVariable.Balance = balance

    GlobalVariable.CreditorEntRoleID = creditornumber //GlobalVariable.IsObjected = IsObjected
    //GlobalVariable.DebtorObjectionReason = DebtorObjectionReason
}

CustomKeywords.'com.database.DBConnection.executeQuery'('EXEC[dbo].[USC_ADP_LDSP_IVR] @Itemid =' + GlobalVariable.CaseNumber)

def getpassword = CustomKeywords.'com.database.DBConnection.executeQuery'(('select top 1 * from USP_CommunicationJob where EntityId =' + 
    GlobalVariable.CaseNumber) + ' order by ID desc')

while (getpassword.next()) {
    Object dpassword = getpassword.getObject('DebtorPassword')

    Object kid = getpassword.getObject('KID')

    GlobalVariable.dpassword = dpassword

    GlobalVariable.KID = kid
}

def bankaccount = CustomKeywords.'com.database.DBConnection.executeQuery'('select * FROM USP_SystemCategories C INNER JOIN USP_SystemCategoryTypes T ON T.Id = C.TypeID WHERE t.Description = \'Company Settings\' AND c.Field2 = \'Bank Account No\'')

while (bankaccount.next()) {
    Object BaccountNumber = bankaccount.getObject('Field3')

    GlobalVariable.BankAccountNumber = BaccountNumber
}

def homeURL = ((((GlobalVariable.DPBaseURL+'/ex?u=' + GlobalVariable.ExcaseNumber) + '&p=') + GlobalVariable.dpassword) + 
'&d=home&ln=') + findTestData('BasicData').getValue(2, 3)

GlobalVariable.DPHomeURL = homeURL

def today = new Date().format('MMM dd, yyyy')

WebUI.openBrowser(GlobalVariable.DPHomeURL)

WebUI.maximizeWindow()

WebUI.click(findTestObject('NavigationBar/nvg_Options'))

WebUI.click(findTestObject('Options/nvg_I_want_to_change_my_contact_details'))

WebUI.click(findTestObject('Change_my_contact_details_Elements/lnk_Change my contact details'))

WebUI.verifyElementPresent(findTestObject('What_information_does_FDC_know_about_me_Elements/lbl_Redirecting to bankID Login'), 
    2)

WebUI.verifyElementPresent(findTestObject('What_information_does_FDC_know_about_me_Elements/btn_Confirm'), 0)

WebUI.verifyElementPresent(findTestObject('What_information_does_FDC_know_about_me_Elements/btn_Cancel'), 0)

WebUI.closeBrowser()

