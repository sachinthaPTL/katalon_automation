import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://app-unicornbox-qa.azurewebsites.net/')

WebUI.maximizeWindow()

WebUI.sendKeys(findTestObject('PA/txtEmail'), 'sachinthas@unicorn-solutions.com')

WebUI.click(findTestObject('PA/btnNext'), FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Page_Sign in to your account/txtPassword'), 'Schinthaka91')

WebUI.click(findTestObject('Page_Sign in to your account/btnYes'))

WebUI.click(findTestObject('Page_Sign in to your account/btnSignIn'))

WebUI.sendKeys(findTestObject('Page_UnicornBOX/txtSearch'), 'Invoice:  Case No:8783')

WebUI.sendKeys(findTestObject('Page_UnicornBOX/txtSearch'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Page_UnicornBOX/CaseNo'))

WebUI.waitForAngularLoad(7)

not_run: WebUI.scrollToPosition(0, 50)

WebUI.scrollToElement(findTestObject('Page_UnicornBOX/button_Payment Agreement_ant-btn ant-btn-default ant-btn-circle ant-btn-lg case-action-pay-agreement ant-tooltip-open'), 
    5)

WebUI.click(findTestObject('Page_UnicornBOX/button_Payment Agreement_ant-btn ant-btn-default ant-btn-circle ant-btn-lg case-action-pay-agreement ant-tooltip-open'))

not_run: WebUI.waitForAngularLoad(20)

not_run: WebUI.click(findTestObject('Page_UnicornBOX/li_Agreements'))

WebUI.click(findTestObject('Page_UnicornBOX/FinalPayDate'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_UnicornBOX/div_20'))

WebUI.sendKeys(findTestObject('Page_UnicornBOX/MainAmount'), Keys.chord(Keys.CONTROL, Keys.ENTER, Keys.ENTER, Keys.BACK_SPACE))

not_run: WebUI.clearText(findTestObject('Page_UnicornBOX/MainAmount'), FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Page_UnicornBOX/MainAmount'), '1425.48')

WebUI.sendKeys(findTestObject('Page_UnicornBOX/CollectionAmount'), Keys.chord(Keys.CONTROL, Keys.ENTER, Keys.ENTER, Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Page_UnicornBOX/CollectionAmount'), '50')

WebUI.scrollToElement(findTestObject('Page_UnicornBOX/SMS'), 0)

WebUI.click(findTestObject('Page_UnicornBOX/SMS'))

WebUI.click(findTestObject('Page_UnicornBOX/button_Save'))

WebUI.verifyElementText(findTestObject('Page_UnicornBOX/div_Successfully added'), 'Successfully added')

WebUI.click(findTestObject('Page_UnicornBOX/ArrowBack'))

WebUI.scrollToPosition(20, 0)

WebUI.verifyElementText(findTestObject('Page_UnicornBOX/strong_PaymentAgreement'), 'PaymentAgreement')

WebUI.click(findTestObject('Page_UnicornBOX/span_Sachintha Sri Lakmal'))

WebUI.click(findTestObject('Page_UnicornBOX/span_Logout'))

